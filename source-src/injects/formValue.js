function FormValue() {
    const vm = this
    
    vm.plano = ""
    vm.numberCartao = ""
    vm.numberVal = ""
    vm.numberSecurity = ""
    vm.name = ""
}

export default {
    name: 'FormValue',
    fn: FormValue
}