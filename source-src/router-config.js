function routers($stateProvider, $locationProvider, $urlRouterProvider, $compileProvider) {

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    })

    $stateProvider
        .state('home', {
            url: '/home',
            title: 'home',
            component: 'homeComponent',
            resolve: {
                loadPlugin: ($ocLazyLoad) => $ocLazyLoad.load(['views/css/home.css'])
            }
        })
        .state('home.stage-1', {
            url: '/stage-1',
            title: 'stage1',
            component: 'stage1Component',
            resolve: {
                loadPlugin: ($ocLazyLoad) => $ocLazyLoad.load(['views/css/radio-credit.css'])
            }
        })
        .state('home.stage-2', {
            url: '/stage-2',
            title: 'stage2',
            component: 'stage2Component'
        })
        .state('home.stage-3', {
            url: '/stage-3',
            title: 'stage3',
            component: 'stage3Component'
        })

    $urlRouterProvider.otherwise('/home/stage-1')

}

export default routers