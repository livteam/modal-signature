Stage1Ctrl.$inject = []

function Stage1Ctrl() {
    const vm = this

    vm.button = 'Próximo passo: pagamento'
    vm.formData = {}

    vm.$onInit = activate

    function activate() {
        vm.formData = vm.parent.getData()
    }
}

export default {
    name: 'stage1Ctrl',
    fn: Stage1Ctrl
}