HomeCtrl.$inject = ['FormValue']

function HomeCtrl(FormValue) {
    const vm = this
    
    vm.title = "Welcome to Your AngularJS App"
    vm.formData = new FormValue()

    vm.$onInit = activate
    vm.getData = getData

    function activate() {
        console.log('loaded!')
    }
    function getData() {
        return vm.formData
    }
}

export default {
    name: 'homeCtrl',
    fn: HomeCtrl
}