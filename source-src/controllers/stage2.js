Stage2Ctrl.$inject = []

function Stage2Ctrl() {
    const vm = this

    vm.button = 'Concluir meu pagamento'
    vm.numberCartaoTXT = 'Número do Cartão'
    vm.numberValTXT = 'Validade'
    vm.numberSecurityTXT = 'Número de Segurança'
    vm.nameTXT = 'Nome do Titular'
    vm.formData = {}

    vm.$onInit = activate

    function activate() {
        vm.formData = vm.parent.getData()
    }

    vm.isResult = function(value) {
        return $.isNumeric(value)
    }
}

export default {
    name: 'stage2Ctrl',
    fn: Stage2Ctrl
}