Stage3Ctrl.$inject = []

function Stage3Ctrl() {
    const vm = this

    vm.button = 'Visualizar meu anúncio'
    vm.formData = {}

    vm.$onInit = activate

    function activate() {
        vm.formData = vm.parent.getData()
    }
}

export default {
    name: 'stage3Ctrl',
    fn: Stage3Ctrl
}