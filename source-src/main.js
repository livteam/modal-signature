// Dependências do NodeJS
import angular from 'angular'
import uiRouter from '@uirouter/angularjs'
import oclazyload from 'oclazyload'

import components from './components'
import controllers from './controllers'
import directives from './directives'
import injects from './injects'

import jQuery from 'jquery/dist/jquery.slim'

// Dependências Externas
import constants from './constants'
import routers from './router-config'
import init from './init-config'

// Expondo Global
window.$ = window.jQuery = jQuery

// Iniciar o aplicativo
const App = angular.module('myApp', [uiRouter, oclazyload])

angular.element(document).ready( () => {
    angular.bootstrap(document, ['myApp'])
})

// Bootstrap Application
Object.keys(components).forEach((key) => {
    App.component(components[key].name, components[key]);
})
Object.keys(controllers).forEach((key) => {
    App.controller(controllers[key].name, controllers[key].fn);
})
Object.keys(directives).forEach((key) => {
    App.directive(directives[key].name, directives[key].fn);
})
Object.keys(injects).forEach((key) => {
    App.value(injects[key].name, injects[key].fn);
})
App.constant('AppSettings', constants)
App.config(routers)
App.run(init)

export default App