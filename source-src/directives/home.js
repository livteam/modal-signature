function HomeDirective() {
    return {
        templateUrl: 'views/directives/home.html',
    }
}

export default {
    name: 'homeDirective',
    fn: HomeDirective
}