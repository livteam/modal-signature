export default {
    name: 'stage1Component',

    controller: 'stage1Ctrl as vm',
    templateUrl: 'views/stage-1.html',
    require: {
        parent: '^homeComponent'
    },
    bindings: {
        formData: '<'
    }
}