import home from './home'
import stage1 from './stage1'
import stage2 from './stage2'
import stage3 from './stage3'

const componentsModule = {
    home,
    stage1,
    stage2,
    stage3
}

export default componentsModule