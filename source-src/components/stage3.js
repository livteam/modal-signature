export default {
    name: 'stage3Component',

    controller: 'stage3Ctrl as vm',
    templateUrl: 'views/stage-3.html',
    require: {
        parent: '^homeComponent'
    },
    bindings: {
        formData: '<'
    }
}