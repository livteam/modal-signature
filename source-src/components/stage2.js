export default {
    name: 'stage2Component',

    controller: 'stage2Ctrl as vm',
    templateUrl: 'views/stage-2.html',
    require: {
        parent: '^homeComponent'
    },
    bindings: {
        formData: '<'
    }
}