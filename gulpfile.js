// Plugins
var gulp = require('gulp'),
    del = require('del'), // Deleta Diretórios e Arquivos
    plumber = require('gulp-plumber'), // Previne Execução de stream
    gutil = require('gulp-util'), // Cria LOG's
    notify = require('gulp-notify'), // Informe das alterações
    changed = require('gulp-changed'), // Só passar arquivos alterados
    watch = require('gulp-watch'), // Verifica alterações em tempo real, caso haja, compacta novamente o projeto
    replace = require('gulp-replace'), // Substitui uma sequência de caracteres dentro de um arquivo
    less = require('gulp-less'), // Compila o LESS para CSS
    cleanCSS = require("gulp-clean-css"), // Minifica o CSS e Remove os comentários
    uglify = require("gulp-uglify"), // Transforma o javascript em formato ilegível para humanos
    concat = require("gulp-concat"), // Agrupa todos os arquivos em um
    browserify = require('browserify'), // É uma biblioteca que pode ser para gerenciar as dependências entre arquivo JS
    babelify = require('babelify'), // Executa babelify para transformar es6 para es5
    source = require('vinyl-source-stream'), // Utiliza fluxos de texto convencional no início do gulp ou vinyl pipelines
    imagemin = require('gulp-imagemin'), // Minifica às imagens
    cache = require('gulp-cache'), // Cache de imagens, então únicas imagens alteradas são compactadas
    pngcrush = require('imagemin-pngcrush'), // Reduz a cor e a profundidade de bits sem perda de qualidade
    htmlmin = require('gulp-htmlmin'), // Minifica os HTML
    htmlreplace = require('gulp-html-replace'), // Substituir os blocos de construção em HTML
    connect = require('gulp-connect'); // Servidor Web local com LiveReload (Alterações em tempo real)

// Raiz
var paths = {
    'destDir': './public/',
    'less': './source-less/main.less',
    'css': [
        './bower_components/normalize-css/normalize.css',
        './bower_components/roboto-fontfacekit/roboto.css',
        './bower_components/ionicons/css/ionicons.css',
        './bower_components/bootstrap/docs/assets/css/bootstrap.css',
        './bower_components/bootstrap/docs/assets/css/bootstrap-responsive.css',
        './source-css/*.css',
    ],
    'fonts': './bower_components/roboto-fontfacekit/fonts/**/*',
    'icons': './bower_components/Ionicons/fonts/*',
    'js': [
        './source-js/*.js',
        './bower_components/bootstrap/docs/assets/js/bootstrap.js'
    ],
    'img': './source-img/*.{png,jpg,ico}',
    'html': './index.html',
    'template': './views/**/*',
    'angular': './source-src/main.js'
}

// Error notifications
function reportError(error) {

    // Pretty error reporting
    var report = '\n',
        chalk = gutil.colors.white.bgRed;

    if (error.plugin) {
        report += chalk('[PLUGIN]:') + ' ' + error.plugin + '\n';
    }
    if (error.message) {
        report += chalk('[ERROR]\040:') + ' ' + error.message + '\n';
    }

    console.error(report);

    // Notification
    if (error.line && error.column) {
        var notifyMessage = '(Linha: ' + error.line + ':' + error.column + ') -- ';
    } else {
        var notifyMessage = '';
    }

    notify({
        title: 'FALHA: ' + error.plugin,
        message: notifyMessage + 'Consulte o Console.',
        sound: 'Sosumi'
    }).write(error);

    gutil.beep(); // System beep (backup)

    // Prevent the 'watch' task from stopping
    this.emit('end');
}

/* Tarefas | command: gulp --tasks
–––––––––––––––––––––––––––––––––––––––––––––––––– */
function Clean() {
    return del([paths.destDir]);
}

function Less() {
    return gulp.src(paths.less)
        .pipe(plumber({
            errorHandler: reportError
        }))
        .pipe(less())
        .pipe(plumber.stop())
        .pipe(gulp.dest('./source-css/'))
        .pipe(notify({
            onLast: true,
            title: 'Gulp',
            subtitle: 'success',
            message: 'Compilado LESS para CSS com sucesso',
            sound: 'Pop'
        }));
}

function Css() {
    return gulp.src(paths.css)
        .pipe(plumber({
            errorHandler: reportError
        }))
        .pipe(changed(paths.destDir + 'css/'))
        .pipe(cleanCSS({
            advanced: true,
            aggressiveMerging: true,
            compatibility: '*',
            level: 2
        }))
        .pipe(concat('main.min.css'))
        .pipe(replace('../roboto-fontfacekit/fonts/', 'fonts/'))
        .pipe(replace('../ionicons/fonts/', 'fonts/ionicons/'))
        .pipe(plumber.stop())
        .pipe(gulp.dest(paths.destDir + 'css/'))
        .pipe(notify({
            onLast: true,
            title: 'Gulp',
            subtitle: 'success',
            message: 'Minificado o CSS com sucesso',
            sound: 'Pop'
        }))
        .pipe(connect.reload());
}

function Fonts() {
    return gulp.src(paths.fonts)
        .pipe(gulp.dest(paths.destDir + 'css/fonts'))
        .pipe(notify({
            onLast: true,
            title: 'Gulp',
            subtitle: 'success',
            message: 'Copiado às Fontes Web com sucesso',
            sound: 'Pop'
        }));
}

function Icons() {
    return gulp.src(paths.icons)
        .pipe(gulp.dest(paths.destDir + 'css/fonts/ionicons'))
        .pipe(notify({
            onLast: true,
            title: 'Gulp',
            subtitle: 'success',
            message: 'Copiado às Icons Web com sucesso',
            sound: 'Pop'
        }));
}

function Browserify() {
    return browserify({
            entries: paths.angular,
            debug: false,
            transform: [
                [babelify, {
                    presets: ["es2015", "stage-2"],
                    plugins: ["transform-runtime"]
                }]
            ]
        })
        .bundle() // Empacota os dados do arquivo em um fluxo de texto (gulp só entende fluxo de vinil)
        .pipe(plumber({
            errorHandler: reportError
        }))
        .pipe(source('bundle.js')) // Converte o fluxo para vinil
        .pipe(plumber.stop())
        .pipe(gulp.dest('./source-js/'))
        .pipe(notify({
            onLast: true,
            title: 'Gulp',
            subtitle: 'success',
            message: 'Compilado Framework Front-end usando Browserify com sucesso',
            sound: 'Pop'
        }));
}

function Javascript() {
    return gulp.src(paths.js)
        .pipe(plumber({
            errorHandler: reportError
        }))
        .pipe(changed(paths.destDir + 'js/'))
        //.pipe(concat('bundle.min.js'))
        .pipe(uglify({
            mangle: false
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest(paths.destDir + 'js/'))
        .pipe(notify({
            onLast: true,
            title: 'Gulp',
            subtitle: 'success',
            message: 'Minificado o Javascript com sucesso',
            sound: 'Pop'
        }))
        .pipe(connect.reload());
}

function Img() {
    return gulp.src(paths.img)
        .pipe(changed(paths.destDir + 'img/'))
        .pipe(cache(imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true,
            svoPlugins: [{
                removeViewBox: false
            }],
            use: [pngcrush()]
        })))
        .pipe(gulp.dest(paths.destDir + 'img/'))
        .pipe(notify({
            onLast: true,
            title: 'Gulp',
            subtitle: 'success',
            message: 'Minificado às Imagens com sucesso',
            sound: 'Pop'
        }))
        .pipe(connect.reload());
}

function Html() {
    return gulp.src(paths.html)
        .pipe(changed(paths.destDir))
        .pipe(htmlreplace({
            'css': 'css/main.min.css',
            'js': [
                'js/bundle.js',
                'js/bootstrap.js'
            ]
        }))
        .pipe(htmlmin({
            collapseBooleanAttributes: true,
            collapseWhitespace: true,
            decodeEntities: true,
            html5: true,
            minifyCSS: true,
            minifyJS: true,
            processConditionalComments: true,
            removeAttributeQuotes: true,
            removeComments: true,
            removeEmptyAttributes: true,
            removeOptionalTags: true,
            // removeRedundantAttributes: true,
            removeScriptTypeAttributes: true,
            removeStyleLinkTypeAttributes: true,
            removeTagWhitespace: true,
            sortAttributes: true,
            sortClassName: true,
            useShortDoctype: true
        }))
        .pipe(replace('source-img/', 'img/'))
        .pipe(gulp.dest(paths.destDir))
        .pipe(notify({
            onLast: true,
            title: 'Gulp',
            subtitle: 'success',
            message: 'Minificado HTML5 com sucesso',
            sound: 'Pop'
        }))
        .pipe(connect.reload());
}

function Template() {
    return gulp.src(paths.template)
        .pipe(changed(paths.destDir + 'views/'))
        .pipe(htmlmin({
            collapseBooleanAttributes: true,
            collapseWhitespace: true,
            decodeEntities: true,
            html5: true,
            minifyCSS: true,
            minifyJS: true,
            processConditionalComments: true,
            removeAttributeQuotes: true,
            removeComments: true,
            removeEmptyAttributes: true,
            removeOptionalTags: true,
            // removeRedundantAttributes: true,
            removeScriptTypeAttributes: true,
            removeStyleLinkTypeAttributes: true,
            removeTagWhitespace: true,
            sortAttributes: true,
            sortClassName: true,
            useShortDoctype: true
        }))
        .pipe(replace('source-img/', 'img/'))
        .pipe(gulp.dest(paths.destDir + 'views/'))
        .pipe(notify({
            onLast: true,
            title: 'Gulp',
            subtitle: 'success',
            message: 'Minificado Template HTML5 com sucesso',
            sound: 'Pop'
        }))
        .pipe(connect.reload());
}

function Connect() {
    connect.server({
        root: 'public',
        port: 8080,
        host: 'localhost',
        https: false,
        livereload: true,
        name: 'modal-assignature',
        debug: true
    });
}

function Whatch() {
    gulp.watch(paths.less, gulp.parallel(Less))
    gulp.watch(paths.css, gulp.parallel(Css));
    gulp.watch(paths.angular, gulp.parallel(Browserify));
    gulp.watch(paths.js, gulp.parallel(Javascript));
    gulp.watch(paths.img, gulp.parallel(Img));
    gulp.watch(paths.html, gulp.parallel(Html));
    gulp.watch(paths.template, gulp.parallel(Template));
}

exports.Clean = Clean;
exports.Less = Less;
exports.Css = Css;
exports.Fonts = Fonts;
exports.Icons = Icons;
exports.Browserify = Browserify;
exports.Javascript = Javascript;
exports.Img = Img;
exports.Html = Html;
exports.Template = Template;
exports.Connect = Connect;
exports.Whatch = Whatch;

/* Sequências
–––––––––––––––––––––––––––––––––––––––––––––––––– */
var styles = gulp.series(Less, Css),
    scripts = gulp.series(Browserify, Javascript),
    copy = gulp.parallel(Img, Html, Template, Fonts, Icons),
    serve = gulp.parallel(Connect, Whatch);

gulp.task('styles', styles);
gulp.task('scripts', scripts);
gulp.task('copy', copy);
gulp.task('serve', serve);

// Tarefa padrão quando executado o comando `gulp`
gulp.task('default', gulp.series(Clean, styles, scripts, copy, serve));