# modal-assignature

> Teste de **Front-end** elaborado pela *Jaime Camara* em **15 de Agosto de 2017** para a atividade de demonstrar competências técnicas em Angular.

------



[TOC]

## Requisitos

> - [AngularJS](#ANGULARJS): **v1.6.6**
> - [UI-router](#UI-ROUTER): **v1.0.6**
> - [jQuery](#JQUERY): **v3.2.1**
> - [Bower](#BOWER): **v3.10.10**
> - [Gulp](#GULP): **v4.0**

> - **Bower [DEPENDÊNCIAS]:**
>
>   - [normalize-css](https://necolas.github.io/normalize.css/): v7.0.0
>   - [roboto-fontfacekit](http://fontfacekit.github.com/roboto): v1.5.1
>   - [bootstrap](http://getbootstrap.com/2.3.2/): v2.3.2
>   - [Ionicons](http://ionicons.com): v2.0.1
>
> - **Estrutura:**
>
>   - **source-less:** Estrutura de arquivos LESS
>   - **source-css:** Junção de todos os arquivos CSS do (Bower/source-less)
>   - **source-img:** Imagens (*.{png,jpg,ico})
>   - **source-src:** Projeto Angular e jQuery com/ou sem às bibliotecas adicionais
>   - **source-js:** Junção de todos os arquivos JS (Bower/source-src)
>   - **views:** Templates do Angular (*.{html,css})
>   - **public:** Resultado
>
> - **Editores:**
>
>   Visual Studio Code

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification (paste: public)
npm run build
```

For more information see the [docs for AngularJS](https://angularjs.org).

## Informações

Foi construido de acordo com o repositório https://github.com/gjcdigital/Front-test e com layout mais próximo do seguinte site https://front-teste.firebaseapp.com/

Ambos com acesso a **28 de Agosto de 2016**.

## Direitos autorais e Licença

Este trabalho não foi modificado de seus Criadores (Link's de consulta abaixo), foi adaptado de acordo com a documentação do mesmo e dando os créditos contida neste repositório, a busca e a organização para futuras atualizações deve ser dado ao **contributors.txt** (BY).

Este trabalho foi escrito por Leonardo Cavalcante Carvalho e está licenciado com uma [Licença **MIT**](https://opensource.org/licenses/MIT).

[^stackedit]: [StackEdit](https://stackedit.io/) is a full-featured, open-source Markdown editor based on PageDown, the Markdown library used by Stack Overflow and the other Stack Exchange sites.

